'''
    File name: fsm.py
    Author: Krzysztof Galus
    Date created: 12/7/2020
    Date last modified: 14/1/2020
    Python Version: 3.8.5
    Purpose: class for finite state machines
'''

__all__ = ['FSM']
__author__ = "Krzysztof Galus"

__license__ = "Public domain"
__email__ = "krzysztof.galus.k7@gmail.com"

from itertools import product
from random import randint, choice, shuffle, sample
import matplotlib.pyplot as plt 
#from graphviz import Digraph
from hashlib import sha3_256
import os

dirname = os.path.dirname(__file__) + '\\visualisations\\'

class FSM:
    def __init__(self):
        self.states = set()
        self.input_alphabet = set()
        self.output_alphabet = set()
        self.starting_state = None
        self.transition = {}

    def __call__(self, input_stream):
        current_state = self.starting_state
        for part in input_stream:
            current_state, output = self.transition[current_state, part] 
            yield output

    def __str__(self):
        return f"states: {self.states} input:{self.input_alphabet} output:{self.output_alphabet} "
                
    def visualisation(self, file):

        g = Digraph('G', filename=file)
        
        try:
            g.node(self.starting_state, shape='doublecircle')
        
            for input_transition, output_transition in self.transition.items():
                input_, output_ = input_transition[1],output_transition[1]
                g.edge(input_transition[0], output_transition[0], label=f'{input_}, {output_}')
            
        except TypeError:
            g.node(str(self.starting_state, "utf-8") , shape='doublecircle')
            
            for input_transition, output_transition in self.transition.items():
                input_, output_ = input_transition[1],output_transition[1]
                g.edge(str(input_transition[0],"utf-8") , str(output_transition[0],"utf-8") , label=f'{input_}, {output_}')
            
        g.view()

    def __matmul__(self, other):
        fsm = self.__class__()
        assert self.complete() and other.complete()
        for element in other.output_alphabet:
            if not element in self.input_alphabet: raise ValueError(f"{element}") 
        for i in self.states:
            for j in other.states:
                fsm.states.add(i + "." + j)
                for k in other.input_alphabet:
                    temp1 = other.transition[j, k]
                    temp2  = self.transition[i, temp1[1]]
                    fsm.transition.update({(i+"."+j, k):(temp2[0]+"."+temp1[0], temp2[1])})
        fsm.starting_state = self.starting_state + "." + other.starting_state
        fsm.input_alphabet = other.input_alphabet
        fsm.output_alphabet =  self.output_alphabet
        assert fsm.complete()
        return fsm
    
    def complete(self):
        for state in self.states:
            for input_ in self.input_alphabet:
                if not (state, input_) in self.transition:
                    return False
        return True
    
    def random(self, n_state):
        output_alphabet = [x for x in self.output_alphabet]
        states = [f"{i}" for i in range(n_state)]
        self.transition.clear()
        for state in states:
            for input_ in self.input_alphabet:
                self.transition.update({(state, input_):(choice(states), choice(output_alphabet))})
        self.states = states
        self.starting_state = choice(states)  
        assert self.complete()                  

    def __repr__(self): #TODO
        pass
    
    def obfuscate(self):
        mixing_way = self.mixing_init()
        states = [self.mixing_function(state, mixing_way) for state in self.states]

        self.states = set(states)
        transition = {}
        for input_transition, output_transition in self.transition.items():
            
            new_input_transition = self.mixing_function(input_transition[0], mixing_way)
            new_output_transition = self.mixing_function(output_transition[0], mixing_way)
            transition.update({(new_input_transition, input_transition[1]):(new_output_transition, output_transition[1])})
            
        self.transition = transition
        
        self.starting_state = self.mixing_function(self.starting_state, mixing_way)
        

    
class FSM_hash(FSM):
        
    def mixing_init(self):
        return sha3_256(str(self).encode('utf-8')).digest()
        
    def mixing_function(self, value, salt):
        hashed = sha3_256(value.encode('utf-8') + str(salt).encode('utf-8'))
        return hashed.hexdigest()[:4]

class FSM_bijection(FSM):
        
    def mixing_init(self):
        bijection= {}
        random_ = [x for x in range(0,len(self.states))]
        for i, element in enumerate(self.states):
            bijection.update({element : f"{random_[i]}"})
        return bijection
            
    def mixing_function(self, value, bijection):
        return bijection[value]


if __debug__ and __name__ == "__main__":
    a = FSM_hash()
    a.input_alphabet = set([True,False])
    a.output_alphabet = set([True,False])
    a.random(3)
    b = FSM_hash()
    b.input_alphabet = set([True,False])
    b.output_alphabet = set([True,False])
    b.random(4)

    msg = [1,1]
    a_b = a @ b

    # a.visualisation(dirname + 'a1.gv')
    # b.visualisation(dirname + 'b1.gv')
    
    # a_b.visualisation(dirname + 'a@b1.gv') 
    
    # a_b.visualisation(dirname + 'before1.gv')
    assert list(a(b(msg))) == list(a_b(msg))
    
    a_b.obfuscate()
    # a_b.visualisation(dirname + 'after1.gv')

    assert list(a(b(msg))) == list(a_b(msg))