from random import randint, choice
from modulo import Modulo
from copy import copy
from tqdm import tqdm
from term import Term

class Vector:
    def __init__(self, values, gf = None):
        self.gf = gf
        try:
            self.length = len(values)
            if gf:
                self.values = [Modulo(gf, value) for value in values]
            else:
                self.values = values
        except (TypeError, AttributeError):
            if gf:
                self.values = [Modulo(gf, values),]
            else:
                self.values = [values,]
            self.length = 1

    def __getitem__(self, index):
        try:
            start, stop, step = index.start, index.stop, index.step
            return self.__class__(self.values[start:stop:step])

        except AttributeError:
            return self.values[index]

    def __mul__(self, other):
        return self.__class__([x*y for x,y in zip(self.values, other.values)], gf=self.gf)

    def __iter__(self):
        yield from self.values

    def __str__(self):
        return f'Vector{self.values}'

    def __len__(self):
        return len(self.values)

    def get_dimension(self):
        return (1, len(self.values))

    @classmethod
    def var(cls, name, length):
        return cls([Term.var(f'{name}_{i}') for i in range(length)])

    @classmethod
    def random(cls, length, gf):
        return cls([randint(0, gf - 1) for _ in range(length)], gf=gf)

    def __eq__(self, other):
        try:
            if not self.length == other.length:
                raise ValueError('Vectors must have the same length')

            for left, right in zip(self.values, other.values):
                if left != right:
                    return False
            return True
        except (TypeError, AttributeError):
            try:
                return self.values[0] == other
            except (TypeError, AttributeError):
                return self == other.values[0]

    def __hash__(self):
        return hash((self.length, *self.values, self.gf))

    def __or__(self, other):
        if not self.gf == other.gf:
            raise ValueError
        return self.__class__(self.values + other.values, gf=self.gf)

    def __setitem__(self, index, value):
        self.values[index] = value

    def __matmul__(self ,other):
        values = []
        for j in range(len(self.values)):
            result = []
            for A, B in zip(self.values, other[:j]):
                result.append(A * B)
            values.append(sum(result))
        return self.__class__(values)

    def __add__(self, other):
        return self.__class__([element1 + element2 for element1, element2 in zip(self.values, other.values)])

    __radd__ = __add__

    def __call__(self, **kwargs):
        if kwargs:
            values = []
            for element in self.values:
                values.append(element(**kwargs))
            return self.__class__(values)
        else:
            return self.__class__(self.values)

class Matrix:
    def __init__(self, dimension = (4,4), gf = None, values = None):
        self.row_dimension = dimension[0]
        self.col_dimension = dimension[1]
        self.gf = gf
        if gf:
            try:
                self.values = values
                map(lambda x: Modulo(gf, x), values)
            except:
                self.values = [[Modulo(gf, 0) for col in range(self.col_dimension)] for row in range(self.row_dimension)]
        else:
            if values:
                self.values = values
            else:
                self.values = [[0 for col in range(self.col_dimension)] for row in range(self.row_dimension)]

    def __str__(self):
        string = ''
        for row in self.values:
            string += f'{row}\n'
        return string

    def __mul__(self, multiplier):
        multiplied = self.__class__(self.get_dimension())
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                multiplied.values[i][j] = self[i,j] * multiplier
        return multiplied

    __rmul__ = __mul__

    def __matmul__(self, other):
        
        matrix = self.__class__(dimension=(self.get_dimension()[0], other.get_dimension()[1]), gf=self.gf)

        for i, row in enumerate(self.values):
            for j in range(len(row)):
                result = []
                for A, B in zip(row, other[:j]):
                    result.append(A * B)
                matrix.values[i][j] = sum(result)
        return matrix
        
    def __getitem__(self, index):

        if index == Ellipsis:
            return copy(self)

        try:
            start, stop, step = index.start, index.stop, index.step
            if start != None and not stop:
                try:
                    return Vector(self.values[start][::step])
                except IndexError:
                    return Vector(self.values[start::step])
            if not start and stop != None:
                values = []
                for i, row in enumerate(self.values):
                    if step:
                        if i % step:
                            values.append(row[stop])
                    else:
                        values.append(row[stop])
                return Vector(values)

        except (TypeError, AttributeError):
            try:
                i, j = index
                start_left, stop_left, step_left = i.start, i.stop, i.step
                if start_left == None: start_left : 0
                if stop_left == None: stop_left = self.row_dimension
                start_right, stop_right, step_right = j.start, j.stop, j.step
                if start_right == None: start_right : 0
                if stop_right == None: stop_right = self.col_dimension

                values = []
                for i in range(start_left, stop_left):
                    if step_left:
                        if (i - start_left) % step_left:
                             values.append(self.values[i][start_right:stop_right:step_right])
                    else:
                        values.append(self.values[i][start_right:stop_right:step_right])
                return self.__class__(dimension=(stop_left - start_left, stop_right - start_right), gf=self.gf, values=values)

            except (TypeError, AttributeError):
                i, j = index
                try:
                    return self.values[i][j]
                except (TypeError, AttributeError):
                    return self.values[j]

    def __add__(self, other):
        assert self.get_dimension() == other.get_dimension()
        matrix = self.__class__(self.get_dimension())
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                matrix.values[i][j] = self[i,j] + other[i,j]
        return matrix

    __radd__ = __add__

    def __sub__(self, other):
        assert self.get_dimension() == other.get_dimension()
        matrix = self.__class__(self.get_dimension())
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                matrix.values[i][j] = self[i,j] - other[i,j]
        return matrix

    __rsub__ = __sub__

    @classmethod
    def zero(cls, dimension, gf = None):
        matrix = cls(dimension, gf)
        for i in range(matrix.row_dimension):
            for j in range(matrix.col_dimension):
                if i == j:
                    if matrix.gf:
                        matrix.values[i][j] = Modulo(matrix.gf, 0)
                    else:
                        matrix.values[i][j] = 0
        return matrix

    @classmethod
    def unit(cls, dimension, gf = None):
        matrix = cls(dimension, gf)
        for i in range(matrix.row_dimension):
            for j in range(matrix.col_dimension):
                if i == j:
                    if matrix.gf:
                        matrix.values[i][j] = Modulo(matrix.gf, 1)
                    else:
                        matrix.values[i][j] = 1
        return matrix

    def __eq__(self, other):
        if not self.get_dimension() == self.get_dimension():
            raise ValueError('Different dimensions')
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                 if self[i,j] != other[i,j]:
                     return False
        return True

    @classmethod
    def random_triangular(cls, dimension, gf = None, variant='lower'):
        if variant == 'lower':
            matrix = cls(dimension, gf)
            for i in range(matrix.row_dimension):
                for j in range(matrix.col_dimension):
                    if matrix.gf:
                        if i > j and choice([True, False]):
                            matrix.values[i][j] = Modulo(matrix.gf, 1)
                        elif i == j:
                            matrix.values[i][j] = Modulo(matrix.gf, 1)
                    else:
                        if i > j and choice([True, False]):
                            matrix.values[i][j] = 1
                        elif i == j:
                            matrix.values[i][j] = 1
            return matrix
        elif variant == 'upper':
            matrix = cls(dimension, gf)
            for i in range(matrix.row_dimension):
                for j in range(matrix.col_dimension):
                    if matrix.gf:
                        if i < j and choice([True, False]):
                            matrix.values[i][j] = Modulo(matrix.gf, 1)
                        elif i == j:
                            matrix.values[i][j] = Modulo(matrix.gf, 1)
                    else:
                        if i < j and choice([True, False]):
                            matrix.values[i][j] = 1
                        elif i == j:
                            matrix.values[i][j] = 1
            return matrix

    def get_dimension(self):
        return (self.row_dimension, self.col_dimension)
    
    def is_zero(self):
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                if self[i,j] != 0:
                    return False
        return True

    def is_square(self):
        return self.row_dimension == self.col_dimension

    def is_diagonal(self):
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                if self[i,j] and i != j:
                    return False
        return True

    def is_orthogonal(self):
        return self.T() @ self == self.unit(self.get_dimension(), self.gf)

    @classmethod
    def random_orthogonal_gf2(cls, dimension):
        """
        Method generates random orthogonal matrix in gf(2)
        https://arxiv.org/pdf/2005.14649v1.pdf
        """

        sequence = [Modulo(2, choice((0,1))) for _ in range(dimension[0] - 1)]
        if sum(sequence) == 1:
            sequence.append(Modulo(2,1))
        else:
            sequence.append(Modulo(2,0))

        assert sum(sequence) == 0

        matrix = cls(dimension=dimension, gf=2)
        for i in range(dimension[0]):
            for j in range(dimension[1]):
                matrix.values[i][j] = sequence[j] - sequence[i]

        assert matrix @ matrix.T() == cls.zero(dimension= dimension, gf=2)

        skew_symmetric_matrix = cls(dimension=dimension, gf=2)
        for i in range(dimension[0]):
            for j in range(dimension[1]):
                skew_symmetric_matrix.values[i][j] = -matrix[j,i]
        
        orthogonal = skew_symmetric_matrix + cls.unit(dimension=dimension, gf=2)

        return orthogonal

    def is_lower_triangular(self):
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                if self[i,j] and i < j:
                    return False
        return True

    def is_upper_triangular(self):
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                if self[i,j] and i > j:
                    return False
        return True

    def is_triangular(self):
        return sum(self.is_lower_triangular(), self.is_upper_triangular())

    def det(self):
        if not self.is_square():
            raise ValueError('Only a square matrix has a determinant')
        result = 0
        if self.row_dimension == 1:
            return self[0,0]
        for j in range(self.row_dimension):
            result += ((-1) ** (0 + j)) * self[0,j] * self.minor(0, j).det()
        return result
        
    def minor(self, i, j):
        if not self.is_square():
            raise ValueError('Only a square matrix has a minor')
        minor = self.__class__((self.row_dimension - 1, self.col_dimension - 1))
        minor.values = [[x for idx, x in enumerate(row) if idx != j] for idx, row in enumerate(self.values) if idx != i]
        return minor

    def adj(self):
        adjugate = self.__class__(dimension=self.get_dimension())
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                adjugate.values[i][j] = ((-1)**(i + j)) * self.minor(i,j).det()
        return adjugate
    
    def inversion(self):
        if not self.is_square():
            raise ValueError('Only a square matrix has an inversion')
        if self.det() == 0:
            raise ZeroDivisionError('Deteminant cannot be 0')
        return self.adj().T() * (1/self.det())

    def T(self):
        transposed = self.__class__(dimension=self.get_dimension())
        for i in range(self.row_dimension):
            for j in range(self.col_dimension):
                transposed.values[i][j] = self[j,i]
        return transposed

    @classmethod
    def random_lower_triangular(cls, dimension, gf = None):

        straight = cls.random_triangular(dimension, gf, 'lower')
        strict = straight - cls.unit(dimension, gf)
        inverse = cls(dimension, gf)
        power = cls.unit(dimension, gf)
        sign = 1

        while power != cls(dimension, gf):
            inverse += sign * power
            power @= strict
            sign = -sign

        return straight, inverse

    @classmethod
    def random_upper_triangular(cls, dimension, gf = None):
        pass


if __debug__ and __name__ == '__main__':
    # matrix = Matrix(dimension=(3,3))
    # matrix.values = [[randint(1,5) for _ in range(3)] for row in range(matrix.row_dimension)]
    # Matrix.triangular_inverse((4,4), gf=2)
    vec = Vector((1,2,3.3,45,24,523,234))
    assert vec[-1] == 234
    assert vec[1] == 2
    assert vec[1:] == Vector((2,3.3,45,24,523,234))
    assert vec[1::2] == Vector((2,45,523))
    assert vec[:2] == Vector((1,2))
    assert vec[:2:2] == Vector((1))
    assert vec[:2:2] == 1
    assert vec[1:4] == Vector((2,3.3,45))
    assert vec[1:4:2] == Vector((2,45))

    matrix = Matrix(dimension=(3,3))
    matrix.values = [[1,2,3], [3,2,1], [1,2,3]]

    assert matrix[1,1] == 2
    assert matrix[1:] == Vector((3,2,1))
    assert matrix[:1] == Vector((2,2,2))
    assert matrix[1:,1:] == Matrix(dimension=matrix.get_dimension(), gf=matrix.gf, values=((2,1),(2,3)))
    matrix = Matrix(dimension=(2,2), gf=2, values=((1,0),(0,1)))
    assert matrix.is_orthogonal() == True
    Matrix.random_orthogonal_gf2(dimension=(4,4))
    
    for _ in tqdm(range(100)):
        assert Matrix.random_orthogonal_gf2(dimension=(100,100)).is_orthogonal()