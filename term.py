'''
    File name: Term.py
    Author: Krzysztof Galus
    Date created: 10/12/2020
    Date last modified: 10/12/2020
    Python Version: 3.7
'''

from __future__ import annotations

__all__ = ['Term']
__author__ = "Krzysztof Galus"
__email__ = "krzysztof.galus.k7@gmail.com"
__license__ = "Public domain"

from enum import Enum
from itertools import product
from random import choice
from copy import copy
from modulo import Modulo
from typing import Union

class Term:
    class Type(Enum):
        VAR = 1
        CONST = 2
        ADD = 3
        SUB = 4
        MUL = 5

    def __init__(self, expression_type : Term.Type, variable: str = None, constant : int = None):
        self.variable = variable
        self.constant = constant
        self.tree = []
        self.expression_type = expression_type

    @classmethod
    def const(cls, constant: int):
        term = cls(Term.Type.CONST, None, constant)
        return term

    @classmethod
    def var(cls, variable: str):
        term = cls(Term.Type.VAR, variable, None)
        return term

    def __call__(self, **kwargs):
        assert self.is_valid()
        if kwargs:
            if self.is_const():
                pass
            elif self.is_var():
                if self.variable in kwargs:
                    self.expression_type = Term.Type.CONST
                    self.constant = kwargs.get(self.variable).constant
                    self.variable = None
            else:
                self.tree[1](**kwargs)
                self.tree[2](**kwargs)
        return self

    def __add__(self, other: Union[Term, int]):
        if other.is_const():
            if other.constant == 0:
                return copy(self)
        try:
            if not other.is_valid(): raise ValueError("Wrong value")
        except AttributeError:
            return self + self.const(other)
        except TypeError:
            return self + self.const(other)
        term = self.__class__(self.Type.ADD)
        term.tree.append("+")
        term.tree.append(self)
        term.tree.append(other)
        return term

    def __sub__(self, other: Union[Term, int]):
        if other.is_const():
            if other.constant == 0:
                return copy(self)
        try:
            if not other.is_valid(): raise ValueError("Wrong value")
        except (AttributeError, TypeError):
            return self - self.const(other)
        term = self.__class__(self.Type.SUB)
        term.tree.append("-")
        term.tree.append(self)
        term.tree.append(other)
        return term

    def __mul__(self, other: Union[Term, int]):
        try:
            if not other.is_valid(): raise ValueError("Wrong value")
        except AttributeError:
            return self * self.const(other)
        except TypeError:
            return self * self.const(other)
        term = self.__class__(self.Type.MUL)
        term.tree.append("*")
        term.tree.append(self)
        term.tree.append(other)
        return term

    __rmul__ = __mul__
    
    __radd__ = __add__
    
    __rsub__ = __sub__

    def __str__(self):
        assert self.is_valid()
        if self.is_const():
            return str(self.constant)
        elif self.is_var():
            return str(self.variable)
        elif self.is_add() or self.is_sub() or self.is_mul():
            return "(" + str(self.tree[1]) + self.tree[0] + str(self.tree[2]) + ")"
        raise RuntimeError("I should not be here")

    def __eq__(self, other: Union[Term, int]):
        
        variables = self.variables() | other.variables()
        
        values = [x for x in self.domain()]
        
        if len(variables) == 0:
            return self.evaluate().constant == other.evaluate().constant
        
        for i in product(values, repeat=len(variables)):
            if self(**dict(zip(variables, i))).evaluate() != other(**dict(zip(variables, i))).evaluate():
                return False

        return True
    
    def evaluate(self):
        value = self.take_values()
        return self.const(value)
    
    def take_values(self):
        if self.is_var():
            raise ValueError('While evaluating there can not be any variable')
        elif self.is_const():
            return self.constant
        elif self.is_add():
            return self.tree[1].take_values() + self.tree[2].take_values()    
        elif self.is_sub():
            return self.tree[1].take_values() - self.tree[2].take_values()   
        elif self.is_mul():
            return self.tree[1].take_values() * self.tree[2].take_values() 
        raise RuntimeError('Term has wrong type or it has no type')
    
    @classmethod
    def domain(cls):
        yield cls.const(0)
        yield cls.const(1)
    
    @classmethod
    def random(cls, variables: str, degree: int):
        
        def check_if_reached(operations, max_degree):
            degree = 0
            for operation in operations:
                if degree == max_degree:
                    return True
                if operation == '*':
                    degree += 1
                else:
                    degree = 0
            
            return degree == max_degree
                
        operations = []
        while not check_if_reached(operations, degree):
            operations.append(choice(['*', "+", '-']))
        
        term = cls(Term.Type.VAR, choice(variables))
        for operation in operations:
            if operation == '*':
                term = term * cls.var(choice(variables))
            elif operation == '+':
                term = term + cls.var(choice(variables))
            else:
                term = term - cls.var(choice(variables))
                
        return term
        

    # def count(self, arg):
    #     try:
    #         if not self.is_valid(): raise ValueError("Wrong value")
    #     except AttributeError:
    #         return self.var(self).count(arg)
    #     except TypeError:
    #         return self.var(self).count(arg)
    #     if self.is_var() and self.variable == arg:
    #         return 1
    #     elif self.is_var() and self.variable != arg:
    #         return 0
    #     elif self.is_const():
    #         return 0
    #     elif self.is_add() or self.is_mul() or self.is_sub():
    #         return self.tree[1].count(arg) + self.tree[2].count(arg)

    def is_const(self):
        return self.expression_type == Term.Type.CONST

    def is_var(self):
        return self.expression_type == Term.Type.VAR

    def is_add(self):
        return self.expression_type == Term.Type.ADD

    def is_sub(self):
        return self.expression_type == Term.Type.SUB

    def is_mul(self):
        return self.expression_type == Term.Type.MUL

    def variables(self):
        assert self.is_valid()
        if self.is_const():
            return set()
        elif self.is_var():
            return set([self.variable])
        elif self.is_add() or self.is_mul() or self.is_sub():
            return self.tree[1].variables() | self.tree[2].variables()
        raise RuntimeError("I should not be here")

    def is_valid(self):
        if sum([self.is_add(), self.is_var(), self.is_const(), self.is_mul(), self.is_sub()]) != 1:
            return False
        if self.is_add() or self.is_sub() or self.is_mul():
            return self.tree[1].is_valid() and self.tree[2].is_valid()
        elif self.is_var():
            return self.variable != None
        elif self.is_const():
            return self.constant != None
        raise RuntimeError("I should not be here")


if __debug__ and __name__ == '__main__':
    x = Term.var("x")
    y = Term.var("y")
    z = Term.var("z")
    g = x + y
    f = x + y
    assert Term.var("x").variables() == set("x")
    assert Term.const(0).variables() == set()
    assert (x+y).variables() == {"x", "y"}
    assert (x+(y*z)).variables() == {"x", "y", "z"}
    # assert f(x=1, y=2) == 3
    # assert Term.const(7) == 7
    # assert 8 == Term.const(8)
    # assert Term.const(7) != 8
    # assert Term.var("x") != "abc"
    # assert Term.var(9) != "abc"

    # d = Term.random(['a','b','c'], 2)
    
    x = (Term.const(1) + Term.const(1) * Term.const(0)).evaluate()
    
    Term.const(Modulo(2,1)) * Term.const(Modulo(2,1)) == Term.const(Modulo(2,0)) + Term.const(Modulo(2,1))
    
    try:
        Term.const(0) * Term.var('x') == Term.const(0)
    except ValueError:
        print('It should be here')
    