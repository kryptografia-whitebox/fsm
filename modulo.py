'''
    File name: modulo.py
    Author: Krzysztof Galus
    Date created: 10/28/2020
    Date last modified: 11/13/2020
    Python Version: 3.8.5
    Purpose: class for modular arithmetics
'''
from __future__ import annotations

__all__ = ['Modulo']
__author__ = "Krzysztof Galus"
__license__ = "Public domain"
__email__ = "krzysztof.galus.k7@gmail.com"


from itertools import product
from math import sqrt, ceil
from typing import Union

def naive_is_prime(arg: int):
    for element in range(2, ceil(sqrt(arg)) + 1):
        if (arg % element == 0):
            return False
    return True

class Modulo:
    def __init__(self, base: int, value: int):
        self.base = base
        self.value = value

    def __add__(self, other: Modulo):
        try:
            if not self.base == other.base:
                raise ValueError("Wrong bases")
        except (AttributeError, TypeError):
            return self.__class__(self.base, (self.value + other) % self.base)
        return self.__class__(self.base, (self.value + other.value) % self.base)

    def __sub__(self, other: Modulo):
        try:
            if not self.base == other.base:
                raise ValueError("Wrong bases")
        except (AttributeError, TypeError):
            return self.__class__(self.base, self.value + (-other))
        return self + (-other)

    def __rsub__(self, other: Modulo):
        try:
            if not self.base == other.base:
                raise ValueError("Wrong bases")
        except (AttributeError, TypeError):
            return self.__class__(self.base, other + (-self.value))
        return self.__class__(self.base, other.value - self.value)

    def __neg__(self):
        if self.value > 0:
            return self.__class__(self.base, self.base - self.value)
        elif self.value == 0:
            return self.__class__(self.base, 0)

    def __invert__(self):
        return self + 1

    def __or__(self, other: Modulo):
        return self + other + self * other

    def __str__(self):
        return f"{self.value} mod {self.base}"

    __repr__ = __str__

    def __mul__(self, other):
        try:
            if not self.base == other.base:
                raise ValueError("Wrong bases")
        except (AttributeError, TypeError):
            return self.__class__(self.base, self.value * other % self.base)
        return self.__class__(self.base, self.value * other.value % self.base)

    __rmul__ = __mul__
    __radd__ = __add__

    def __int__(self):
        return self.value

    def __hash__(self):
        return hash((self.value, self.base))

    def __eq__(self, other):
        try:
            if self.base != other.base:
                return False
            if self.value != other.value:
                return False
        except (AttributeError, TypeError):
            return int(self) == other
        return True

    def __pow__(self, exponent: int):
        residue = self.value
        result = 1
        for _ in range((exponent).bit_length()):
            if (exponent & 1):
                result *= residue
                result %= self.base
            residue = (residue ** 2) % self.base
            exponent >>= 1
        return self.__class__(self.base, result)

    def __divmod__(self, other: int):
        if int(other) == 0:
            raise ZeroDivisionError
        residue = self.__class__(self.base, 1)
        quotient = self.__class__(self.base, 1)
        for i in range(1, self.base):
            if i == 1:
                residue = self - i * other
            else:
                residue_temp = self - i * other
                if int(residue) > int(residue_temp) and int(residue_temp) >= 0:
                    residue = residue_temp
                    quotient = Modulo(self.base, i)
        return quotient, residue

    def __floordiv__(self, other: int):
        m, r = divmod(self, other)
        return m

    def __mod__(self, other: int):
        try:
            m, r = divmod(self, other)
        except TypeError:
            m, r = divmod(self.__class__(other.base, self), other)
        return r

    def __rmod__(self, other: int):
        try:
            m, r = divmod(self, other)
        except TypeError:
            m, r = divmod(self, self.__class__(self.base, other))
        return r
    
    def inverse(self):
        u = 1
        w = self.value
        x = 0
        z = self.base
        while(w != 0):
            if w < z:
                u, x = x, u
                w, z = z, w
            q = w//z
            u -= q*x
            w -= q*z
        if z != 1:
            raise ValueError("This value cant be 0")
        if x < 0:
            x += self.base
        return self.__class__(self.base, x)

if __debug__ and __name__ == "__main__":
    # asserts
    assert Modulo(8,7).inverse() == 7
    for base in range(2, 17):
        all_values1 = (Modulo(base, value) for value in range(base))
        all_values2 = (Modulo(base, value) for value in range(base))
        all_values3 = (Modulo(base, value) for value in range(base))
        for a, b, c in product(all_values1, all_values2, all_values3):
            assert a + b == b + a
            assert (a + b) + c == a + (b + c)
            assert a + 0 == a
            assert a + 1 != a
            assert a + (-a) == 0

            if a != b:
                assert a + (-b) != 0

            assert a - b == a + (-b)

            assert a * b == b * a
            assert a * (b * c) == (a * b) * c
            assert a * (b + c) == a * b + a * c

            if a != 0 and naive_is_prime(base):
                assert 1 % a == 0


#           ASSERTS RELATED TO DIVISION
            if b != 0:
                m, r = divmod(a, b)

                assert a == m * b + r

                for k in range(1, a.base):
                    assert int(a - (k * b)) >= int(r)

                assert a // b == m
                assert a % b == r
            else:
                try:
                    m, r = divmod(a, b)
                except ZeroDivisionError:
                    pass
                else:
                    assert False, "should raise ZeroDivisionError"
