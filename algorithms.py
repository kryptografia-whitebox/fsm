'''
    File name: algorithms.py
    Author: Krzysztof Galus
    Date created: 12/7/2020
    Date last modified: 14//2020
    Python Version: 3.8.5
    Purpose: 
'''

__all__ = ['algorithms']
__author__ = "Krzysztof Galus"
__license__ = "Public domain"
__email__ = "krzysztof.galus.k7@gmail.com"

from fsm import FSM_bijection, FSM_hash
from itertools import zip_longest, combinations
from matrix import Matrix, Vector
from term import Term
from random import randint


class Bit:
    def __init__(self, value):
        self.value = value

    def __add__(self, other):
        try:
            return self.__class__((self.value + other.value) % 2)
        except (TypeError, AttributeError):
            return self.__class__((self.value + other) % 2)

    __radd__ = __add__

    def __mul__(self, other):
        try:
            return self.__class__((self.value * other.value) % 2)
        except (TypeError, AttributeError):
            return self.__class__((self.value * other) % 2)

    def __str__(self):
        return f'Bit {self.value}'

    __repr__ = __str__

    __rmul__ = __mul__

    def __or__(self, other):
        return self + other + self * other

    def __invert__(self):
        return self + 1
    
    def __eq__(self, other):
        try:
            return self.value == other.value
        except (TypeError, AttributeError):
            return self.value == other

    def __int__(self):
        return self.value

    def __mod__(self, other):
        return self.value % other

class FSM:
    def __init__(self, output_fn, state_fn, starting_state, state_1_len):
        self.output_fn = output_fn
        self.state_fn = state_fn
        self.starting_state = starting_state
        self.state_1_len = state_1_len
        self.state_length = len(starting_state)

    def __call__(self, input_stream):
        current_state = self.starting_state
        for input_args in input_stream:
            print(current_state)
            yield self.output_fn(input_args, current_state)
            current_state = self.state_fn(input_args, current_state)

    def __matmul__(self, other):
        
        A = Matrix.random_orthogonal_gf2(dimension=(self.state_length + other.state_length, self.state_length + other.state_length))
        A_inv = A.T()
        B, B_inv = Matrix.random_lower_triangular(dimension=(self.state_length + other.state_length, self.state_length + other.state_length), gf=2)

        def state_fn(input_char, state):
            state_1 = state[:self.state_1_len]
            state_2 = state[self.state_1_len:]
            state_prod_prev = other.state_fn(input_char, state_2)
            output_prod = other.output_fn(input_char, state_prod_prev)
            state_prod_next = self.state_fn(output_prod, state_1)
            return state_prod_next + state_prod_prev
        
        def output_fn(input_char, state):
            state_1 = state[:self.state_1_len]
            state_2 = state[self.state_1_len:]
            return self.output_fn(other.output_fn(input_char, state_1), state_2)

        combined_fsm = self.__class__(output_fn, state_fn, Vector(self.starting_state + other.starting_state), self.state_1_len)
        combined_fsm.state_fn = lambda input_, state: Vector((state_fn(input_, state @ (A_inv @ B_inv)))) @  (A @ B)
        combined_fsm.output_fn = lambda input_, state: output_fn(input_, ( state @ (A_inv @ B_inv)))
        self._random_nonlinear(2)
        return combined_fsm

    def _random_nonlinear(self, length):
        b1 = Vector.var('a', length)

        values = []
        values.append(Term.const(0))
        for i in range(1,length):
            part1 = Term.var('a_' + str(randint(0,i - 1)))
            part2 = Term.var('a_' + str(randint(0,i - 1)))

            if part1 == part2:
                values.append(part1)

            else:
                values.append(part1 * part2)
        b2 = Vector(values)

        b = b1 + b2

        subs = {}

        a = Vector([Term.const(0) for _ in range(length)])
        a1 = Vector.var('b', length)

        for i in range(length):
            a[i] = a1[i] + b2[i](**subs)
            subs['a_' + str(i)] = a[i]

        assert Term.var('a_0') + Term.const(0) == (Term.var('a_0') + Term.const('0')) + Term.const(0)
        print('hej')
        e = a

        d = a(**{'b_' + str(i) : b[i] for i in range(length)})

        assert b(**{'a_' + str(i) : a[i] for i in range(length)}) == a1
        assert a(**{'b_' + str(i) : b[i] for i in range(length)}) == b1

def mul3(x):
    return 3 * x

def mul3_generator(bits):
    before = Bit(0)
    one_forward = Bit(0)
    for bit in bits:
        yield output_fn(bit, before, one_forward)
        one_forward, before = state_fn_mul(bit, before, one_forward)

def add(x, y):
    return x + y

def int_to_bits(x, n_zeros = 1):
    for i in range(x.bit_length()):
        yield Bit((x >> i) & 1)
    for _ in range(n_zeros): yield Bit(0)

def bits_to_int(bits):
    result = 0
    it = 0
    for bit in bits:
        result += (1 << it) * int(bit) 
        it += 1
    return result

def output_fn(input_, carry):
    return (input_[0] + input_[1] + carry,)

def output_fn_mul(input_, carry):
    return (sum(carry) + input_[0],)

def state_fn_mul(carry, input_):
    return ((~input_[0] * input_[1] * carry[0]) | (input_[0] * ~input_[1] * carry[0]) | (input_[0] * input_[1] * ~carry[0]) | (input_[0] * input_[1] * carry[0]), input_[0])

def state_fn_add(input_, carry):
    return ((~input_[0] * input_[1] * carry) | (input_[0] * ~input_[1] * carry) | (input_[0] * input_[1] * ~carry) | (input_[0] * input_[1] * carry),)

def output_fn_compare(input_, state):
    return ((input_[0] * ~input_[1]), (~input_[0] * input_[1]),)

def state_fn_compare(input_, state):
    return ((input_[0] * ~input_[1]),)

def add_bits_generator(bits):
    one_forward = False
    for x, y in bits:
        x, y = int(x), int(y)
        if x + y + one_forward == 0:
            yield Bit(0)
            one_forward = False
        elif x + y + one_forward < 2:
            yield Bit(1)
            one_forward = False
        elif x + y + one_forward == 2:
            yield Bit(0)
            one_forward = True
        else:
            yield Bit(1)
            one_forward = True
    # else:
    #     if one_forward: yield Bit(1)

def add_2(bits):
    one_forward = Bit(0)
    for x, y in bits:
        yield output_fn(x, y, one_forward)
        one_forward = state_fn_add(x, y, one_forward)

if __debug__ and __name__ == "__main__":
    #ASSERTS

    bit = Bit(1)
    assert bit + 1 == 0
    assert ~bit == 0
    assert bit * 0 == 0
    assert bit + bit == 0

    # bit1 = Bit(0)
    # bit2 = Bit(1)
    # # assert 1 == int(state_fn(bit, bit1, bit2))
    # assert add(2,3) == int(bits_to_int(list(add_bits_generator(zip(int_to_bits(2), int_to_bits(3))))))
    # assert add(31,123) == bits_to_int(list(add_bits_generator(zip_longest(int_to_bits(31), int_to_bits(123), fillvalue=0))))
    # assert add(31,123) == bits_to_int(list(add_2(zip_longest(int_to_bits(31), int_to_bits(123), fillvalue=0))))
    fsm = FSM(output_fn_compare, state_fn_compare, (Bit(0),), 1)
    #assert add(31,123) == bits_to_int(x[0] for x in fsm((x,) for x in int_to_bits(31)))

    # assert mul3(31) == bits_to_int(x[0] for x in fsm((x,) for x in int_to_bits(31)))
    # assert mul3(31) == bits_to_int(x for x in mul3_generator(int_to_bits(31)))
    # assert mul3(131) == bits_to_int(x for x in mul3_generator(int_to_bits(131)))
    #p = [x for x in fsm(((x,) for x in int_to_bits(3)))]
    # p = [x for x in fsm(zip_longest(int_to_bits(10), int_to_bits(3), fillvalue=Bit(0)))]
    # print(p)
    fsm1 = FSM(output_fn, state_fn_add, (Bit(0),), 1)
    # p = [x for x in fsm1(zip_longest(int_to_bits(10), int_to_bits(3), fillvalue=Bit(0)))]
    # print(p)
    fsm2 = FSM(output_fn_mul, state_fn_mul, (Bit(0),Bit(1),), 2)
    # p = [x for x in fsm((x,) for x in int_to_bits(10))]
    # print(p)
    fsm1 = fsm2 @ fsm2
    for x in fsm1((y,) for y in int_to_bits(11)):
        print(x) 